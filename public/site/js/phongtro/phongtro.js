$(document).ready(function () {
    $(document).on('click', '.p_view', function () {
        var room_id = $(this).attr("room_id");
        $.ajax({
            url: "phongtro/view_detail",
            method: "POST",
            data: { room_id: room_id },
            dataType: "json",
            success: function (response) {
                console.log(response.content);
                $(".content").html(response.content);
            }
        });
    });
    $(document).on('change', '.p_quanhuyen', function () {
        var id = $(this).val();
        $.ajax({
            url: "phongtro/get_xaphuong",
            method: "POST",
            data: { id: id },
            dataType: "json",
            success: function (response) {
                var html = "<option value = 0 > Tất cả </option>"
                $.each(response, function (index, value) {
                    if (index == 0) html += "<option value = " + value.id + " selected>" + value.name + "</option>";
                    else html += "<option value = " + value.id + " >" + value.name + "</option>";
                });
                $('.p_xaphuong').html(html);
            }
        });
    });
});