<?php
Class User extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('admin_model');
        $this->load->library("session");
    }
    
    function index()
    {
        if($this->session->userdata('adminInfo')) {
            $this->data['admin'] = $this->session->userdata('adminInfo');
            $this->data['temp'] = 'site2/admin/index';
            $this->load->view('site2/index', $this->data);
        }
        if($this->session->userdata('userInfo')) {
            $this->data['user'] = $this->session->userdata('userInfo');
            $this->data['temp'] = 'site2/user/index';
            $this->load->view('site2/index', $this->data);
        }
    }
    /*
     * Kiểm tra email đã tồn tại chưa
     */
    function check_email()
    {
        $email = $this->input->post('email');
        $where = array('email' => $email);
        //kiêm tra xem email đã tồn tại chưa
        if($this->user_model->check_exists($where))
        {
            //trả về thông báo lỗi
            $this->form_validation->set_message(__FUNCTION__, 'Email đã tồn tại');
            return false;
        }
        return true;
    }
    
    /*
     * Đăng ký thành viên
     */
    function register()
    {    
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        //neu ma co du lieu post len thi kiem tra
        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Tên', 'required|min_length[8]');
            $this->form_validation->set_rules('email', 'Email đăng nhập', 'required|valid_email|callback_check_email');
            $this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]');
            $this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu', 'matches[password]');
            $this->form_validation->set_rules('phone', 'Số điện thoại', 'required');
            $this->form_validation->set_rules('address', 'Địa chỉ', 'required');
  
            //nhập liệu chính xác
            if($this->form_validation->run())
            {
                //them vao csdl
                $password = $this->input->post('password');
                $password = md5($password);
                
                $data = array(
                    'username'    => $this->input->post('email'),
                    'ten'     => $this->input->post('name'),
                    'email'    => $this->input->post('email'),
                    'sdt'    => $this->input->post('phone'),
                    'diachi'  => $this->input->post('address'),
                    'password' => $password,
                    'ngaydk'  => now(),
                );
                if($this->user_model->create($data))
                {
                    //tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Đăng ký thành viên thành công');
                }else{
                    $this->session->set_flashdata('message', 'Không thêm được');
                }
                //chuyen tới trang danh sách quản trị viên
                redirect(site_url());
            }
        }
        
        //hiển thị ra view
        $this->data['temp'] = 'site2/user/register';
        $this->load->view('site2/index', $this->data);
    }
    
    /*
     * Kiem tra đăng nhập
     */
    function login()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        if($this->input->post()) {
            $data_login = $this->input->post();
            $username = $data_login["username"];
            $password = $data_login["password"];
            $this->form_validation->set_rules('username', 'Email đăng nhập', 'required|min_length[6]');
            $this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]');
            // $this->form_validation->set_rules('login' ,'login', 'callback_check_login');
            if($this->form_validation->run()) {
                $input = array('username' => $username);
                if($this->admin_model->get_row($input)) {
                    $data_admin = $this->user_model->get_row($input);
                    if($data_admin->password == md5($password)) {
                        $admindata = array(
                            'username'  => $username,
                            'login' => TRUE
                        );
                        $this->session->set_userdata('adminInfo', $admindata);
                        redirect(base_url()."admin");
                    } 
                }
            }
        }
        
        // //hiển thị ra view
        // $this->data['temp'] = 'site/user/login';
        // $this->load->view('site/layout', $this->data);
    }

    /*
     * Kiem tra email va password co chinh xac khong
     */
    function check_login()
    {
        $user = $this->_get_user_info();
        if($user)
        {
            return true;
        }
        $this->form_validation->set_message(__FUNCTION__, 'Không đăng nhập thành công');
        return false;
    }
    
    /*
     * Lay thong tin cua thanh vien
     */
    private function _get_user_info()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password = md5($password);
        
        $where = array('email' => $email , 'password' => $password);
        $user = $this->user_model->get_info_rule($where);
        return $user;
    }
    
    /*
     * Chinh sua thong tin thanh vien
     */
    // function edit()
    // {
    //     if(!$this->session->userdata('user_id_login'))
    //     {
    //         redirect(site_url('user/login'));
    //     }
    //     //lay thong tin cua thanh vien
    //     $user_id = $this->session->userdata('user_id_login');
    //     $user = $this->user_model->get_info($user_id);
    //     if(!$user)
    //     {
    //         redirect();
    //     }
    //     $this->data['user']  = $user;
        

    //     $this->load->library('form_validation');
    //     $this->load->helper('form');
        
    //     //neu ma co du lieu post len thi kiem tra
    //     if($this->input->post())
    //     {
    //         $password = $this->input->post('password');
            
    //         $this->form_validation->set_rules('name', 'Tên', 'required|min_length[8]');
    //         if($password)
    //         {
    //             $this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]');
    //             $this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu', 'matches[password]');
    //         }
            
    //         $this->form_validation->set_rules('phone', 'Số điện thoại', 'required');
    //         $this->form_validation->set_rules('address', 'Địa chỉ', 'required');
        
    //         //nhập liệu chính xác
    //         if($this->form_validation->run())
    //         {
    //             //them vao csdl
    //             $data = array(
    //                 'name'     => $this->input->post('name'), 
    //                 'phone'    => $this->input->post('phone'),
    //                 'address'  => $this->input->post('address'),
    //             );
    //             if($password)
    //             {
    //                 $data['password'] = md5($password);
    //             }
    //             if($this->user_model->update($user_id, $data))
    //             {
    //                 //tạo ra nội dung thông báo
    //                 $this->session->set_flashdata('message', 'Chỉnh sửa thông tin thành công');
    //             }else{
    //                 $this->session->set_flashdata('message', 'Không thành công');
    //             }
    //             //chuyen tới trang danh sách quản trị viên
    //             redirect(site_url('user'));
    //         }
    //     }
        
    //     //hiển thị ra view
    //     $this->data['temp'] = 'site/user/edit';
    //     $this->load->view('site/layout', $this->data);
    // }
    
    /*
     * Thong tin cua thanh vien
     */
    // function index()
    // {
    //     if(!$this->session->userdata('user_id_login'))
    //     {
    //         redirect();
    //     }
    //     $user_id = $this->session->userdata('user_id_login');
    //     $user = $this->user_model->get_info($user_id);
    //     if(!$user)
    //     {
    //         redirect();
    //     }
    //     $this->data['user']  = $user;
        
    //     //hiển thị ra view
    //     $this->data['temp'] = 'site/user/index';
    //     $this->load->view('site/layout', $this->data);
    // }

    /*
     * Thuc hien dang xuat
     */
    function logout()
    {
        if($this->session->userdata('user_id_login'))
        {
            $this->session->unset_userdata('user_id_login');
        }
        $this->session->set_flashdata('message', 'Đăng xuất thành công');
        redirect();
    }
}

