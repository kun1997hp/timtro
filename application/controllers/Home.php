<?php
class Home extends MY_Controller
{
	function __construct()
    {
        parent::__construct();
        $this->load->library("session");
	}

	function index()
	{
		//lay danh sach slide
		// $this->load->model('slide_model');
		// $slide_list = $this->slide_model->get_list();
		// $this->data['slide_list'] = $slide_list;
		//    $input['order'] = array('buyed', 'DESC');
		//    $product_buy = $this->product_model->get_list($input);
		// $this->data['product_buy']  = $product_buy;

		//lấy nội dung của biến message
		//lấy danh sách phòng trọ
		$this->load->model('phongtro_model');
		$this->load->model('quanhuyen_model');
		$this->load->model('xaphuong_model');

		$input = array();
		// $input['limit'] = array(3, 0);
		$phongtro = $this->phongtro_model->get_list($input);
		$this->data['phongtro'] = $phongtro;

		//lấy danh sách quận huyện
		$input['order'] = array('ID', 'ASC');
		$quanhuyen = $this->quanhuyen_model->get_list($input);
		$this->data['quanhuyen'] = $quanhuyen;



		$message = $this->session->flashdata('message');
		$this->data['message'] = $message;
		$this->data['temp'] = 'site2/newroom';
		$this->load->view('site2/index', $this->data);
	}

	function get_xaphuong($id) {
		$xaphuong = $this->xaphuong_model->get_info($id);
		return $xaphuong;
	}
}
