


<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-1 col-sm-3">
      </div>
      <div class="col-lg-4 col-sm-3">
        <h4>Tin tức</h4>
        <p>Nhận thêm tin mới từ chúng tôi</p>
        <form class="form-inline" role="form">
          <input type="text" placeholder="Nhập địa chỉ email của bạn ở đây" class="form-control">
          <button class="btn btn-success" type="button">Notify Me!</button></form>
        </div>
        <div class="col-lg-3 col-sm-3">
          <h4>Follow</h4>
          <a href="#"><img src="<?php echo public_url()?>/site/images/facebook.png" alt="facebook"></a>
          <a href="#"><img src="<?php echo public_url()?>/site/images/twitter.png" alt="twitter"></a>
          <a href="#"><img src="<?php echo public_url()?>/site/images/linkedin.png" alt="linkedin"></a>
          <a href="#"><img src="<?php echo public_url()?>/site/images/instagram.png" alt="instagram"></a>
        </div>
        <div class="col-lg-4 col-sm-3">
          <h4>Liên hệ</h4>
          <p><b>Bùi Văn Giang</b><br>
            <span class="glyphicon glyphicon-map-marker"></span>KTX Đại học Bách Khoa Hà Nội , Hai Bà Trưng, Hà Nội <br>
            <span class="glyphicon glyphicon-envelope"></span>giangacoustic@gmail.com<br>
            <span class="glyphicon glyphicon-earphone"></span>0349660905</p>
            <p class="copyright">Copyright 2019</p>
          </div>
        </div>
      </div></div>
      <!-- Modal -->
      <div id="loginpop" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="row">
              <div class="col-sm-6 login">
                <h4>Đăng nhập</h4>
                <form class="login-form form_action" method="post" action="<?php echo site_url('user/login')?>" enctype="multipart/form-data">
                          <div class="form-row">
        						<label for="param_username" class="form-label">Username:<span class="req">*</span></label>
        						<div class="form-item">
        							<input type="text" class="input" id="username" name="username">
        							<div class="clear"></div>
        						</div>
        						<div class="clear"></div>
        				  </div>
        				  
        				  <div class="form-row">
        						<label for="param_password" class="form-label">Mật khẩu:<span class="req">*</span></label>
        						<div class="form-item">
        							<input type="password" class="input" id="password" name="password">
        							<div class="clear"></div>
        						</div>
        						<div class="clear"></div>
        				  </div>
        				  <div class="form-row">
        						<label class="form-label">&nbsp;</label>
        						<div class="form-item">
        				           	<input type="submit" class="button" value="Đăng nhập" name="submit">
        						</div>
        				   </div>
                    </form>


                <!-- <form class="login-form" method="post" action="<?php echo site_url('user/login')?>" role="form">
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputPassword2">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> Ghi nhớ
                    </label>
                  </div>
                  <button type="submit" class="btn btn-success">Đăng nhập</button>
                </form>           -->
              </div>
              <div class="col-sm-6">
                <h4>Đăng ký</h4>
                <p>Đăng ký ngay để nhận thêm thông tin về phòng trọ</p>
                <button class="btn btn-info"  onclick="window.location.href='user/register'">Click!</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal -->
    </body>
  </html>



