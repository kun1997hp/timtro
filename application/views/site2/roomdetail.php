<div class="container">
  <div class="properties-listing spacer">

    <div class="row">
      <div class="col-lg-3 col-sm-4 hidden-xs">

        <div class="hot-properties hidden-xs">
          <h4>Hot Properties</h4>
          <div class="row">
            <div class="col-lg-4 col-sm-5"><img src="<?php echo base_url("upload/room/1a.jpeg") ?>" class="img-responsive img-circle"
                alt="properties" /></div>
            <div class="col-lg-8 col-sm-7">
              <h5><a href="property-detail.php">Số 274 Lê Lợi, Ngô Quyền, Hải Phòng</a></h5>
              <p class="price">$300,000</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-sm-5"><img src="<?php echo base_url("upload/room/2a.jpeg") ?>" class="img-responsive img-circle"
                alt="properties" /></div>
            <div class="col-lg-8 col-sm-7">
              <h5><a href="property-detail.php">Số 274 Lê Lợi, Ngô Quyền, Hải Phòng</a></h5>
              <p class="price">$300,000</p>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 col-sm-5"><img src="<?php echo base_url("upload/room/3a.jpeg") ?>" class="img-responsive img-circle"
                alt="properties" /></div>
            <div class="col-lg-8 col-sm-7">
              <h5><a href="property-detail.php">Số 274 Lê Lợi, Ngô Quyền, Hải Phòng</a></h5>
              <p class="price">$300,000</p>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 col-sm-5"><img src="<?php echo base_url("upload/room/4a.jpeg") ?>" class="img-responsive img-circle"
                alt="properties" /></div>
            <div class="col-lg-8 col-sm-7">
              <h5><a href="property-detail.php">Số 274 Lê Lợi, Ngô Quyền, Hải Phòng</a></h5>
              <p class="price">$300,000</p>
            </div>
          </div>

        </div>



        <div class="advertisement">
          <h4>Quảng Cáo</h4>
          <img src="<?php echo public_url() ?>/images/advertisements.jpg" class="img-responsive" alt="advertisement">

        </div>

      </div>

      <div class="col-lg-9 col-sm-8 ">

        <h2>2 phòng ngủ , 1 vệ sinh</h2>
        <div class="row">
          <div class="col-lg-8">
            <div class="property-images">
              <!-- Slider Starts -->
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators hidden-xs">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                  <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                  <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <!-- Item 1 -->
                  <div class="item active">
                    <img src="<?php echo public_url() ?>/images/properties/4.jpg" class="properties" alt="properties" />
                  </div>
                  <!-- #Item 1 -->

                  <!-- Item 2 -->
                  <div class="item">
                    <img src="<?php echo public_url() ?>/images/properties/2.jpg" class="properties" alt="properties" />

                  </div>
                  <!-- #Item 2 -->

                  <!-- Item 3 -->
                  <div class="item">
                    <img src="<?php echo public_url() ?>/images/properties/1.jpg" class="properties" alt="properties" />
                  </div>
                  <!-- #Item 3 -->

                  <!-- Item 4 -->
                  <div class="item ">
                    <img src="<?php echo public_url() ?>/images/properties/3.jpg" class="properties" alt="properties" />

                  </div>
                  <!-- # Item 4 -->
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
                    class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span
                    class="glyphicon glyphicon-chevron-right"></span></a>
              </div>
              <!-- #Slider Ends -->

            </div>




            <div class="spacer">
              <h4><span class="glyphicon glyphicon-th-list"></span>Mô tả</h4>
              <p>Phòng 25m2 , vệ sinh riêng , giờ giấc thoải mái , không chung chủ</p>

            </div>
            <div>
              <h4><span class="glyphicon glyphicon-map-marker"></span> Vị trí</h4>
              <div class="well"><iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0"
                  marginwidth="0"
                  src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Pulchowk,+Patan,+Central+Region,+Nepal&amp;aq=0&amp;oq=pulch&amp;sll=37.0625,-95.677068&amp;sspn=39.371738,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Pulchowk,+Patan+Dhoka,+Patan,+Bagmati,+Central+Region,+Nepal&amp;ll=27.678236,85.316853&amp;spn=0.001347,0.002642&amp;t=m&amp;z=14&amp;output=embed"></iframe>
              </div>
            </div>

          </div>
          <div class="col-lg-4">
            <div class="col-lg-12  col-sm-6">
              <div class="property-info">
                <p class="price"><?php echo $room_info->gia.",000VNĐ" ?></p>
                <p class="area"><span class="glyphicon glyphicon-map-marker"></span><?php echo $room_info->diachi ?></p>

                <div class="profile">
                  <span class="glyphicon glyphicon-user"></span> Agent Details
                  <p>John Parker<br>009 229 2929</p>
                </div>
              </div>

              <h6><span class="glyphicon glyphicon-home"></span> Availabilty</h6>
              <div class="listing-detail">
                <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">5</span> <span
                  data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span
                  data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
                  data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div>

            </div>
            <div class="col-lg-12 col-sm-6 ">
              <div class="enquiry">
                <h6><span class="glyphicon glyphicon-envelope"></span> Post Enquiry</h6>
                <form role="form">
                  <input type="text" class="form-control" placeholder="Full Name" />
                  <input type="text" class="form-control" placeholder="you@yourdomain.com" />
                  <input type="text" class="form-control" placeholder="your number" />
                  <textarea rows="6" class="form-control" placeholder="Whats on your mind?"></textarea>
                  <button type="submit" class="btn btn-primary" name="Submit">Send Message</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>